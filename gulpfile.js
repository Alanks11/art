var gulp = require('gulp');
var browserSync = require('browser-sync');
var fileinclude = require('gulp-rigger');
var uglify = require('gulp-uglify-es').default;
var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var del = require('del');
var pug = require('gulp-pug');
var babel = require('gulp-babel');
var names = require('gulp-filenames');
var write = require('gulp-header');
var fs = require('fs');
var mq = require('gulp-merge-media-queries');

gulp.task('html', function () {
	browserSync.notify('Compiling HTML');
	return gulp.src(['app/*.pug', '!app/_*.pug'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'HTML',
				message: err.message
			}))
		}))
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('css', function () {
	return gulp.src(['app/scss/**/*.scss', '!app/scss/**/_*.scss', 'app/scss/**/*.sass', '!app/scss/**/_*.sass', '!app/scss/libs*.scss'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'CSS',
				message: err.message
			}))
		}))
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(mq())
		.pipe(cssnano({
			reduceIdents: false
		}))
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
			cascade: true
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream())
});

gulp.task('css-libs', function () {
	return gulp.src('app/scss/libs*.scss')
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'CSS Libs',
				message: err.message
			}))
		}))
		// .pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(cssnano({
			reduceIdents: false
		}))
		// .pipe(sourcemaps.write())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream())
});

gulp.task('js', function () {
	browserSync.notify('Compiling internal JS');

	return gulp.src(['app/js/**/*.js', '!app/js/**/_*.js', '!app/js/libs*.js'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'JS',
				message: err.message
			}))
		}))
		.pipe(fileinclude())
		.pipe(sourcemaps.init())

		.pipe(babel({
			presets: [
				[
					'@babel/env',
					{
						targets: {
							edge: "17",
							firefox: "60",
							chrome: "67",
							safari: "11.1",
							ie: "11"
						},
						//useBuiltIns: "entry"
					}
				]
			],
		}))

		.pipe(sourcemaps.write())
		.pipe(uglify())
		.pipe(gulp.dest('./dist/js'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('js-libs', function () {
	browserSync.notify('Compiling external JS');

	return gulp.src('app/js/libs.js')
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'JS Libs',
				message: err.message
			}))
		}))
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file',
			indent: true
		}))
		// .pipe(sourcemaps.init())
		.pipe(uglify())
		// .pipe(sourcemaps.write())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./dist/js'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('css-build', function () {
	return gulp.src(['app/scss/**/*.scss', '!app/scss/**/_*.scss', 'app/scss/**/*.sass', '!app/scss/**/_*.sass', '!app/scss/libs*.scss'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'CSS',
				message: err.message
			}))
		}))
		.pipe(sass())
		.pipe(mq())
		.pipe(cssnano({
			reduceIdents: false
		}))
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
			cascade: true
		}))
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream())
});

gulp.task('css-libs-build', function () {
	return gulp.src('app/scss/libs*.scss')
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'CSS Libs',
				message: err.message
			}))
		}))
		.pipe(sass())
		.pipe(cssnano({
			reduceIdents: false
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream())
});

gulp.task('js-build', function () {
	browserSync.notify('Compiling internal JS');

	return gulp.src(['app/js/**/*.js', '!app/js/**/_*.js', '!app/js/libs*.js'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'JS',
				message: err.message
			}))
		}))
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file',
			indent: true
		}))
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(gulp.dest('./dist/js'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('js-libs-build', function () {
	browserSync.notify('Compiling external JS');

	return gulp.src('app/js/libs.js')
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'JS Libs',
				message: err.message
			}))
		}))
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file',
			indent: true
		}))
		.pipe(uglify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./dist/js'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('img', function () {
	return gulp.src(['app/img/**/*.*'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'img',
				message: err.message
			}))
		}))
		.pipe(gulp.dest('./dist/img'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('fonts', function () {
	return gulp.src(['app/fonts/**/*.*'])
		.pipe(plumber({
			errorHandler: notify.onError(err => ({
				title: 'fonts',
				message: err.message
			}))
		}))
		.pipe(gulp.dest('./dist/fonts'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('webserver', function () {
	browserSync({
		server: {
			baseDir: './dist'
		},
		notify: true
	});
});

gulp.task('clean', function () {
	return del(['./dist']);
});

gulp.task('watch', function () {
	gulp.watch(['app/scss/**/*.scss', '!app/scss/libs*.scss', 'app/comp/**/*.scss', 'app/comp/**/*.sass', 'app/elements/**/**/*.scss'], gulp.series('css'));
	gulp.watch('app/scss/libs*.scss', gulp.parallel('css-libs'));
	gulp.watch('app/**/*.pug', gulp.series('html'));
	gulp.watch(['app/js/**/*.js', '!app/js/libs*.js', 'app/comp/**/*.js', 'app/elements/**/*.js'], gulp.series('js'));
	gulp.watch('app/js/libs*.js', gulp.series('js-libs'));
	gulp.watch(['app/img/**/*.*'], gulp.parallel('img'));
	gulp.watch(['app/fonts/**/*.*'], gulp.parallel('fonts'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('img', 'fonts', 'css-build', 'css-libs-build', 'js-build', 'js-libs-build'), 'html'));

gulp.task('create', gulp.series('clean', gulp.parallel('img', 'fonts', 'html', 'css', 'css-libs', 'js', 'js-libs')));
gulp.task('default', gulp.series('create', gulp.parallel('webserver', 'watch')));

//==== SEPARATE TASKS ====

// incscss - imports scss-parts in main.scss
gulp.task('getScssComponents', function () {
	return gulp.src('./app/scss/_*.scss')
		.pipe(names('parts'))
});
gulp.task('importScssComponents', function (done) {
	var scssParts = names.get('parts', 'relative');
	var out = '//==== COMPONENTS ====\n';
	for (let i = 0; i < scssParts.length; i++) {
		out += `@import '${scssParts[i].split('.')[0]}';\n`;
	}
	gulp.src('app/scss/main.scss')
		.pipe(write(out))
		.pipe(gulp.dest('app/scss/'));
	done();
})
gulp.task('incscss', gulp.series('getScssComponents', 'importScssComponents'));

//====

gulp.task('getPugComponents', function () {
	return gulp.src(['./app/_*.pug', '!./app/_styles.pug', '!./app/_scripts.pug'])
		.pipe(names('parts'))
});
gulp.task('importPugComponents', function (done) {
	var pugParts = names.get('parts', 'relative');
	var out = '';
	for (let i = 0; i < pugParts.length; i++) {
		out += `include ${pugParts[i].split('.')[0]}\n`;
	}
	gulp.src('app/index.pug')
		.pipe(write(out))
		.pipe(gulp.dest('app/'));
	done();
})
gulp.task('incpug', gulp.series('getPugComponents', 'importPugComponents'));

gulp.task('createScss', function (done) {
	var components = names.get('parts', 'relative');
	for (let i = 0; i < components.length; i++) {
		fs.writeFileSync(`./app/scss/${components[i].split('.')[0]}.scss`, '', {
			flag: 'a+'
		});
	}
	done();
})

gulp.task('imp', gulp.series('getPugComponents', 'createScss', 'importPugComponents', 'importScssComponents'));

