function adaptiveVideo(wrapper, videoFrame) {
    let w = parseInt(videoFrame.getAttribute('width'));
    let h = parseInt(videoFrame.getAttribute('height'));
    let ratio = document.createElement('div');
    ratio.style.paddingTop = Math.ceil(h / (w / 100)) + '%';
    wrapper.append(ratio);
    videoFrame.style.width = "100%";
    videoFrame.style.height = "100%";
    videoFrame.removeAttribute('width');
    videoFrame.removeAttribute('height');
}

function hideEl(el, time) {
    if (window.getComputedStyle(el).display === 'none') return;
    let startOpacity = +window.getComputedStyle(el).opacity;
    let timeStep = time / (1000 / 60);
    let animStep = startOpacity / timeStep;

    function hide() {
        if (startOpacity <= 0) {
            el.style.opacity = 0;
            el.style.display = 'none';
            return;
        };
        startOpacity -= animStep;
        el.style.opacity = startOpacity;
        requestAnimationFrame(hide);
    };
    requestAnimationFrame(hide);
};

function showEl(el, time) {
    if (window.getComputedStyle(el).display !== 'none') return;
    el.style.display = 'block';
    el.style.opacity = 0;
    let startOpacity = 0;
    let timeStep = time / (1000 / 60);
    let animStep = 1 / timeStep;

    function show() {
        if (startOpacity >= 1) {
            el.style.opacity = 1;
            return;
        };
        startOpacity += animStep;
        el.style.opacity = startOpacity;
        requestAnimationFrame(show);
    };
    requestAnimationFrame(show);
};
//body.style.overflow = 'hidden';
//document.querySelector('body').style.overflow = 'hidden';

// let blockPage = function() {
//     return {
//         wrapper: document.querySelector('body'),
//         fixedEls: document.querySelectorAll('.js-fixed') || null,
//         scrollWidth: window.outerWidth - window.innerWidth,
//         blocked: false,
//         block() {
//             console.log(this.fixedEls);
//             this.fixedEls.height = '500px';
//             this.blocked = true;
//             this.wrapper.style.overflow = 'hidden';
//             this.wrapper.style.width = `calc(100vw - ${this.scrollWidth}px)`;
//             if (this.fixedEls) {
//                 for (let i = 0; i < this.fixedEls.length; i++) {
//                     this.fixedEls[i].style.width = `calc(100vw - ${this.scrollWidth}px)`;
//                 }
//             }
//         },
//         unblock() {
//             this.blocked = false;
//             this.wrapper.style.overflow = '';
//             this.wrapper.style.width = '';
//             if (this.fixedEls) {
//                 for (let i = 0; i < this.fixedEls.length; i++) {
//                     this.fixedEls[i].style.width = '';
//                 }
//             }
//         }
//     }
// }();
function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
};

function lockScroll() {
    let body = document.querySelector('body');
    let fixedEls = document.querySelectorAll('.js-fixed') || null;
    if (fixedEls) {
        for (let i = 0; i < fixedEls.length; i++) {
            fixedEls[i].style.width = `calc(100vw - ${getScrollBarWidth()}px)`;
        }
    }
    body.style.overflow = 'hidden';
    body.style.width = `calc(100vw - ${getScrollBarWidth()}px)`;
}

function unlockScroll() {
    let body = document.querySelector('body');
    let fixedEls = document.querySelectorAll('.js-fixed') || null;
    if (fixedEls) {
        for (let i = 0; i < fixedEls.length; i++) {
            fixedEls[i].style.width = '';
        }
    }
    body.style.overflow = '';
    body.style.width = '';
}


function heightsEQ(elements) {
    let maxHeight = 0;
    for (let i = 0; i < elements.length; i++) {
        elements[i].style.height = '';
    }
    for (let i = 0; i < elements.length; i++) {
        if (elements[i].clientHeight > maxHeight) {
            maxHeight = elements[i].clientHeight;
        }
    }
    for (let i = 0; i < elements.length; i++) {
        elements[i].style.height = maxHeight + 'px';
    }
    elements[0].parentNode.style.height = maxHeight + 'px';
}
