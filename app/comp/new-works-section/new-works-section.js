; (function () {
    let sort = document.querySelector('.js-sort-select');
    bricks(); //Function instead Masonry library =)
    window.addEventListener('resize', function () {
        bricks();
    });
    function bricks() {
        let time = 300;
        let gap = 20;
        let bottom = 20;
        let columns = 4;
        let fixedElementOrder = 0;
        if (window.matchMedia("(min-width: 0px) and (max-width: 767px)").matches) {
            columns = 2;
            gap = 5;
            bottom = 10;
            fixedElementOrder = 0;
        } else if (window.matchMedia("(min-width: 768px) and (max-width: 1279px)").matches) {
            columns = 3;
            gap = 10;
            bottom = 15;
            fixedElementOrder = 3;
        } else if (window.matchMedia("(min-width: 1280px)").matches) {
            columns = 4;
            gap = 40;
            bottom = 35;
            fixedElementOrder = 4;
        }
        let wrapper = document.querySelector('.js-grid');
        let fixedElement = document.querySelector('.js-fixed-item') || null;
        let items = document.querySelectorAll('.js-grid-item');
        if (fixedElement) {
            if (fixedElement.style.display !== "none") {
                if (fixedElementOrder == 0) {
                    wrapper.append(fixedElement);
                } else {
                    if (items[fixedElementOrder - 1]) {
                        items[fixedElementOrder - 1].before(fixedElement);
                    }
                }
                fixedElement.classList.add('js-grid-item');
            }
        }
        let elements = document.querySelectorAll('.js-grid-item');
        if (fixedElement) {
            fixedElement.classList.remove('js-grid-item');
        }
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.position = 'absolute';
            elements[i].style.top = '0';
            elements[i].style.left = '0';
            elements[i].style.transition = `transform ${time}ms`;
            elements[i].style.boxSizing = 'border-box';
        }
        wrapper.style.position = 'relative';
        let widthDim = (gap * (columns - 1)) / columns;
        let heights = [];
        let offset = [];
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.width = `calc(${100 / columns}% - ${widthDim}px)`;
            sort.style.width = `calc(${100 / columns}% - ${widthDim}px)`;
            if (fixedElement) {
                fixedElement.style.width = `calc(${100 / columns}% - ${widthDim}px)`;
            }
        }
        for (let i = 0; i < columns; i++) {
            offset.push(0);
        }
        for (let i = 0; i < elements.length; i++) {
            heights.push(elements[i].offsetHeight);
        }
        for (let i = 0; i < elements.length; i += columns) {
            for (let k = 0; k < columns; k++) {
                if (!elements[i + k]) break;
                if (i == 0) {
                    elements[i + k].style.transform = `translate( calc(${k * 100}% + ${k * gap}px), ${offset[k]}px)`;
                    offset[k] = offset[k] + heights[i + k];
                } else {
                    elements[i + k].style.transform = `translate( calc(${k * 100}% + ${k * gap}px), ${offset[k] + bottom}px)`;
                    offset[k] = offset[k] + heights[i + k] + bottom;
                }
            }
        }
        let maxHeight = 0;
        for (let i = 0; i < offset.length; i++) {
            if (offset[i] >= maxHeight) {
                maxHeight = offset[i];
            }
        }
        wrapper.style.height = maxHeight + 'px';
    }
    window.dispatchEvent(new Event('resize'));
    //---lazy load
    let sections = document.querySelectorAll('.lazy-section');
    let preOffset = 300;
    for (let i = 0; i < sections.length; i++) {
        sections[i].lazyImg = sections[i].querySelectorAll('[data-src]');
    }
    lazyLoader();
    function lazyLoader() {
        for (let i = 0; i < sections.length; i++) {
            if (sections[i].getBoundingClientRect().top <= window.innerHeight + preOffset) {
                for (let k = 0; k < sections[i].lazyImg.length; k++) {
                    if (!sections[i].lazyImg[k].getAttribute('src')) {
                        sections[i].lazyImg[k].setAttribute('src', sections[i].lazyImg[k].dataset.src);
                        sections[i].lazyImg[k].onload = bricks;
                    }
                }
            }
        }
    }
    function lazyHandler() {
        window.removeEventListener('scroll', lazyHandler);
        setTimeout(function () {
            window.addEventListener('scroll', lazyHandler);
        }, 300);
        lazyLoader();
    }
    window.addEventListener('scroll', lazyHandler);
}());
