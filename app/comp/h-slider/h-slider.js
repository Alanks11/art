;
(function () {
    let hSlider = new Swiper('.h-slider', {
        direction: 'horizontal',
        loop: true,
        pagination: {
            el: '.h-slider__pagination',
            type: 'bullets',
            clickable: true,
        },
    });
}());