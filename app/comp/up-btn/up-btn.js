
jQbuttonUp($('.js-up-btn'), 400, 500);

function jQbuttonUp(element, threshold, speed) {
    $(document).on('scroll', function () {
        if ($(this).scrollTop() > threshold) {
            element.fadeIn();
        } else {
            element.fadeOut();
        }
    });
    element.click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, speed);
    });
};