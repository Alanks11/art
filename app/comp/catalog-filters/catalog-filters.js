;
(function () {
    //--accordion
    let opened = $('.js-tabs-wrapper').attr('data-opened');
    let media = $('.js-tabs-wrapper').attr('data-media');
    $('.js-tabs-title').each(function (ind) {
        $(this).attr('data-index', ind);
        $(this).on('click', function () {
            let index = $(this).attr('data-index');
            if (index !== opened) {
                $('.js-tabs-item').hide(0);
                $(`.js-tabs-item[data-index="${index}"]`).show(0);
                opened = index;
            }
            $('.js-tabs-title').removeClass('active');
            $(this).addClass('active');
        });
    });
    $('.js-tabs-item').each(function (ind) {
        $(this).attr('data-index', ind);
    });
    $('.js-tabs-subtitle').each(function (ind) {
        $(this).attr('data-index', ind);
        $(this).on('click', function () {
            let index = $(this).attr('data-index');
            if (index !== opened) {
                $('.js-tabs-content').slideUp(300);
                $(`.js-tabs-content[data-index="${index}"]`).slideDown(300);
                opened = index;
            }
        });
    });
    $('.js-tabs-content').each(function (ind) {
        $(this).attr('data-index', ind);
    });
    setStates();
    $(window).on('resize', setStates);
    function setStates() {
        if (window.matchMedia(`(max-width:${media}px)`).matches) {
            $('.js-tabs-item').show(0);
            $('.js-tabs-subtitle').show(0);
            $('.js-tabs-content').slideUp(0);
            $(`.js-tabs-content[data-index="${opened}"]`).slideDown(0);
            $('.js-tabs-header').hide(0);
        } else {
            $('.js-tabs-title').removeClass('active');
            $(`.js-tabs-title[data-index="${opened}"]`).addClass('active');
            $('.js-tabs-subtitle').hide(0);
            $('.js-tabs-item').hide(0);
            $(`.js-tabs-item[data-index="${opened}"]`).show(0);
            $('.js-tabs-content').slideDown(300);
            $('.js-tabs-header').show(0);
        }
    }
    //--minimize
    let minim = document.querySelector('.js-tabs-minimize');
    minim.minimized = true;
    minim.textContent = 'Развернуть фильтр';
    $('.js-tabs-wrapper').stop().hide(300);
    minim.addEventListener('click', function (e) {
        e.preventDefault();
        if (this.minimized) {
            $('.js-tabs-wrapper').slideDown(100);
            this.minimized = false;
            this.textContent = 'Свернуть фильтр';
            this.classList.add('active');
        } else {
            $('.js-tabs-wrapper').slideUp(100);
            this.minimized = true;
            this.textContent = 'Развернуть фильтр';
            this.classList.remove('active');
        }
    })
    //--autocomplete select
    $('.js-autocomplete-select').select2({
        minimumResultsForSearch: Infinity,
        multiple: true,
        dropdownPosition: 'below'
    });
    $('.js-autocomplete-select').on('change', function () {
        var values = $(this).val();
        var $options = $(this).closest('.js-autocomplete').find('.js-autocomplete-options');
        $options.html('');
        for (var i = 0; i < values.length; i++) {
            $options.append('<div class="autocomplete__item js-autocomplete-item">' + values[i] + '</div>');
        }
    });
    $(document).on('click', '.js-autocomplete-item', function () {
        var $select = $(this).closest('.js-autocomplete').find('.js-autocomplete-select');
        var text = $(this).text();
        var currentValues = $select.val();
        var values = currentValues.filter(function (item) {
            return item !== text;
        });
        $select.val(values).trigger('change');
    });
    //filters
    let inputFrom = $("#price-from");
    let inputTo = $("#price-to");
    //cancel enter
    inputFrom.on('keypress', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });
    inputTo.on('keypress', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });
    //sliders
    $("#size-slider").ionRangeSlider();
    let sizeSliderInstance = $("#size-slider").data("ionRangeSlider");
    $("#price-slider").ionRangeSlider({
        onStart: updateInputs,
        onChange: updateInputs,
        onFinish: updateInputs,
        onUpdate: updateInputs,
    });
    let priceSliderInstance = $("#price-slider").data("ionRangeSlider");
    inputFrom.on('change', function (e) {
        let from = +$(this).prop('value');
        let to = +inputTo.prop('value');
        if (from > to || from < priceSliderInstance.options.min) {
            priceSliderInstance.update({
                from: priceSliderInstance.options.min,
            });
        } else {
            priceSliderInstance.update({
                from: from,
            });
        }
    });
    inputTo.on('change', function (e) {
        let to = +$(this).prop('value');
        let from = +inputFrom.prop('value');
        if (from > to || to > priceSliderInstance.options.max) {
            priceSliderInstance.update({
                to: priceSliderInstance.options.max,
            });
        } else {
            priceSliderInstance.update({
                to: to,
            });
        }
    });
    function updateInputs(data) {
        inputFrom.prop('value', data.from);
        inputTo.prop('value', data.to);
    }
    //input reset
    $('.js-input-reset').on('click', function () {
        priceSliderInstance.options.from = priceSliderInstance.options.min;
        priceSliderInstance.options.to = priceSliderInstance.options.max;
        inputFrom.prop('value', priceSliderInstance.options.min);
        inputFrom.attr('value', priceSliderInstance.options.min);
        inputTo.prop('value', priceSliderInstance.options.max);
        inputTo.attr('value', priceSliderInstance.options.max);
        priceSliderInstance.reset();
        sizeSliderInstance.reset();
        $('.js-autocomplete-options').html('');
    });
    //-- scroll init
    $('.js-autocomplete-select').on('select2:open', function () {
        var values = $(this).val();
        var $options = $(this).closest('.js-autocomplete').find('.js-autocomplete-options');
        $options.html('');
        for (var i = 0; i < values.length; i++) {
            $options.append('<div class="autocomplete__item js-autocomplete-item">' + values[i] + '</div>');
        }
        var ps = new PerfectScrollbar('.select2-results__options', {
            wheelPropagation: true,
        });
        $('.select2-results__options').trigger('click');
        $('.select2-results__options').on('psUpdate', function () {
            ps.update();
        });
        setTimeout(function () {
            ps.update();
        }, 100);
    });
    $('div').trigger('psUpdate');
    //minimize filters block
    $('.js-minimize').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('opened');
        $('.js-catalog-filters').stop().slideToggle(300);
        $('.js-tabs-wrapper').stop().slideUp(300);
        minim.minimized = true;
        minim.textContent = 'Развернуть фильтр';
        minim.classList.remove('active');
    });
    $('.js-catalog-filters').slideDown(0);
    $(window).on('resize', function () {
        if (!window.matchMedia('(max-width: 769px)').matches) {
            $('.js-catalog-filters').slideDown(0);
        }
    })
}());