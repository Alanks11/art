//favorite-icon
$('.js-to-favor').on('click', function () {
    $(this).toggleClass('active');
})
//accordion
$('.js-descr-title').on('click', function () {
    let id = $(this).data('id');
    $(this).toggleClass('active');
    $(`.js-descr-content[data-id="${id}"]`).stop().slideToggle(300);
})
//preview-slider
let thumbsDirection = window.matchMedia('(max-width:767px)').matches ? 'horizontal' : 'vertical';
let itemCarousel = new Swiper('#thumbs', {
    direction: thumbsDirection,
    loop: false,
    slidesPerView: 3,
    slideToClickedSlide: true,
    //loopedSlides: 3,
    spaceBetween: 10,
    slidesPerGroup: 1,
    //centeredSlides: true,
    height: 275,
    breakpoints: {
        1280: {
            height: 170,
        }
    },
    slideToClickedSlide: true,
});
window.addEventListener('resize', function () {
    if (window.matchMedia('(max-width:767px)').matches) {
        itemCarousel.changeDirection('horizontal');
    } else {
        itemCarousel.changeDirection('vertical');
    }
});
//big picture slider
let itemMain = new Swiper('#main', {
    direction: 'horizontal',
    loop: true,
    navigation: {
        nextEl: '.slider__main-navigation .js-next',
        prevEl: '.slider__main-navigation .js-prev',
    },
    thumbs: {
        swiper: itemCarousel
    },
    autoHeight: true,
});
//open a-la magnific popup
$('.js-modal').on('click', function (e) {
    $('.slider__main-wrapper').toggleClass('active');
    itemMain.update();
});
