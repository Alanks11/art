; (function () {
    let newsSlider = new Swiper('.js-news-slider', {
        direction: 'horizontal',
        slidesPerView: 2,
        loop: true,
        breakpoints: {
            959: {
                slidesPerView: 1,
            }
        },
        navigation: {
            nextEl: '.art-news .js-next',
            prevEl: '.art-news .js-prev',
        },
    });
}());