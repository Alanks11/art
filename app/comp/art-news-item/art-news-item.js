; (function () {
    let items = document.querySelectorAll('.art-news-item .js-eq-height');
    heightsEQ(items);
    window.addEventListener('resize', function () {
        let resizeTimeout;
        if (!resizeTimeout) {
            resizeTimeout = setTimeout(function () {
                resizeTimeout = null;
                heightsEQ(items);
            }, 1000);
        }
    });
}());