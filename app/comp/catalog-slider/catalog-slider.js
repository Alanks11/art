; (function () {
    let newsSlider = new Swiper('.js-catalog-slider', {
        direction: 'horizontal',
        slidesPerView: 5,
        spaceBetween: 15,
        loop: true,
        breakpoints: {
            1279: {
                slidesPerView: 4,
            },
            767: {
                slidesPerView: 1,
            }
        },
        navigation: {
            nextEl: '.catalog-slider .js-next',
            prevEl: '.catalog-slider .js-prev',
        },
    });
}());