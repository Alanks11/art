;
(function () {

    //--hamburger
    $('.js-hamburger').on('click', function () {
        if (window.getComputedStyle(document.querySelector('body')).getPropertyValue('overflow') === 'hidden') {
            unlockScroll();
        } else {
            lockScroll();
        }
        $('.js-header-bottom').slideToggle(300);
        $(this).toggleClass('header__hamburger--active');
    });

    //more in menu
    // $('.js-more').on('mouseenter', function () {
    //     $('.js-header-bottom').removeAttr('style');
    //     $('.js-hamburger').removeClass('header__hamburger--active');
    // });
    //close menu by click out
    $(document).on('click', function (e) {
        if ((!($('.js-hamburger').is(e.target) || $('.js-hamburger').has(e.target).length > 0)) &&
            (!($('.js-more').is(e.target) || $('.js-more').has(e.target).length > 0))) {
            $('.js-header-bottom').slideUp(300, 'ease', function(){
                $('.js-header-bottom').removeAttr('style');
            });
            if ($('.js-hamburger').hasClass('header__hamburger--active')) {
                unlockScroll();
                $('.js-hamburger').removeClass('header__hamburger--active')
            }
        }
    });
    //--search window
    $('.js-header-search').on('click', function () {
        $('.js-header-search-window').slideToggle(300);
        $('.js-header-search-input').val('');
        $('.js-header-search-input').trigger('focus');
    });
    $('.js-header-search-close').on('click', function () {
        $('.js-header-search-input').val('');
    })
    //close search by click out
    $(document).on('click', function (e) {
        if (!($('.js-header-search').is(e.target) || $('.js-header-search').has(e.target).length > 0)) {
            let block = $('.js-header-search-window');
            if (!block.is(e.target) && block.has(e.target).length == 0) {
                $('.js-header-search-window').slideUp(300);
                $('.js-header-search-input').val('');
            }
        }
    });
    //--user menu
    $('.js-header-user').on('click', function () {
        $('.js-user-menu').fadeToggle(300);
    });
    //close user menu by click out
    $(document).on('click', function (e) {
        if (!($('.js-header-user').is(e.target) || $('.js-header-user').has(e.target).length > 0)) {
            $('.js-user-menu').fadeOut(300);
        }
    });
    //--lang-menu
    $('.js-header-lang').on('click', function () {
        $('.js-lang-menu').fadeToggle(300);
    });
    //close lang menu by click out
    $(document).on('click', function (e) {
        if (!($('.js-header-lang').is(e.target) || $('.js-header-lang').has(e.target).length > 0)) {
            $('.js-lang-menu').fadeOut(300);
        }
    });
    //-POPUPS
    let reg = document.querySelector('.reg');
    let auth = document.querySelector('.auth');
    let pass = document.querySelector('.pass');
    let success = document.querySelector('.success');
    function openStep(obj) {
        $('form').each(function () {
            $(this).trigger('reset');
        });
        reg.style.display = 'none';
        auth.style.display = 'none';
        pass.style.display = 'none';
        success.style.display = 'none';
        obj.style.display = 'block';
    }
    $('.js-header-guest').on('click', function () {
        lockScroll();
        $('.js-popups').fadeIn(300);
        openStep(auth);
    })
    function closeAllModals() {
        $('form').each(function () {
            $(this).trigger('reset');
        });
        valPass.resetForm();
        valAuth.resetForm();
        valReg.resetForm();
        $('.error').removeClass('error');
        $('.js-popups').fadeOut(300);
        unlockScroll();
    }
    $('.js-popups').on('click', function (e) {
        if ((!($('.js-popup-window').is(e.target) || $('.js-popup-window').has(e.target).length > 0))) {
            closeAllModals();
        }
    });
    $('.js-popups-close').on('click', function () {
        closeAllModals();
    });
    $('.js-go-to-auth').on('click', function () {
        openStep(auth);
    });
    $('.js-go-to-reg').on('click', function () {
        openStep(reg);
    });
    $('.js-go-to-pass').on('click', function () {
        openStep(pass);
    });
    let valPass = $('.js-form-pass').validate({
        submitHandler: function () {
            openStep(success);
        }
    });
    let valAuth = $('.js-form-auth').validate();
    let valReg = $('.js-form-reg').validate();
    $.extend($.validator.messages, {
        email: "Введите корректный адрес."
    });
}());

