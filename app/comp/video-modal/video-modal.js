;
(function () {
    let screen = document.querySelector('.js-video-screen');
    let frame = screen.querySelector('iframe');
    let open = document.querySelector('.js-open-video');
    let close = document.querySelector('.js-close-video');
    let video = document.querySelector('.js-video-wrapper');
    //open video in modal
    open.addEventListener('click', function () {
        lockScroll();
        showEl(video, 500);
        document.querySelector('.header').style.display = 'none';
        frame.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
        video.onclick = function (e) {
            clickOut(e);
        }
    });
    //open video 
    close.addEventListener('click', function () {
        document.querySelector('.header').style.display = '';
        frame.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        unlockScroll();
        hideEl(video, 500);
    });
    adaptiveVideo(screen, frame);
    function clickOut(e) {
        if (!(screen == e.target || screen.contains(e.target))) {
            hideEl(video, 500);
            unlockScroll();
            frame.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            document.querySelector('.header').style.display = '';
        }
        video.onclick = null;
    }
}());