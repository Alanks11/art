let reviewSlider = new Swiper('.review__slider-container', {
    loop: true,
    slidesPerView: 2,
    navigation: {
        nextEl: '.review .js-next',
        prevEl: '.review .js-prev',
    },
    spaceBetween: 40,
    breakpoints: {
        1279: {
            spaceBetween: 36,  
        },
        767: {
            slidesPerView: 1,
            spaceBetween: 16,
        }
    }
});