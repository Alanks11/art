let worksSlider = new Swiper('.js-works-slider', {
    slidesPerView: 4,
    spaceBetween: 45,
    loop: true,
    navigation: {
        nextEl: '.item-works .js-next',
        prevEl: '.item-works .js-prev',
    },
    breakpoints: {
        1280: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30,
        }
    },
    preloadImages: false,
    lazy: {
        loadOnTransitionStart: false,
        loadPrevNext: false,
    },
});
